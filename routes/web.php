<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\News;

Route::get('/', [News::class, 'index']);

Route::controller(AuthController::class)->group(function () {
    Route::get('register', 'register')->name('register');
    Route::post('register', 'registerSave')->name('register.save');

    Route::get('login', 'login')->name('login');
    Route::post('login', 'loginAction')->name('login.action');

    Route::get('logout', 'logout')->middleware('auth')->name('logout');
});

Route::middleware('auth')->group(function () {
    Route::get('dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::controller(NewsController::class)->prefix('news')->group(function () {
        Route::get('', 'index')->name('news');
        Route::get('create', 'create')->name('news.create');
        Route::post('store', 'store')->name('news.store');
        Route::get('show/{id}', 'show')->name('news.show');
        Route::get('edit/{id}', 'edit')->name('news.edit');
        Route::put('edit/{id}', 'update')->name('news.update');
        Route::delete('destroy/{id}', 'destroy')->name('news.destroy');
    });

    Route::get('/profile', [App\Http\Controllers\AuthController::class, 'profile'])->name('profile');
});

Route::get('/{slug}', [News::class, 'show']);

