<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class News extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = \App\Models\News::orderBy('created_at', 'DESC')->get();
        return view('news.index', compact('news'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = \App\Models\News::where("slug", $id)->first();

        return view('news.show', compact('post'));
    }

}
