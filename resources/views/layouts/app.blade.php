<!doctype html>
<html lang="en" data-bs-theme="auto">
<head>
    <script src="/docs/5.3/assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Foxpost PróbaMunka</title>

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

</head>
<body>
<header class="container-fluid p-0 m-0">
    <nav class="navbar navbar-expand-lg bg-body-tertiary pt-0 pb-0">
        <a class="navbar-brand pt-0 pb-0" href="#">
            <img src="/images/header-logo-square.svg" alt="Bootstrap">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Hírek</a>
                </li>
                <li class="nav-item">
                    @if (Auth::check())
                    <a class="nav-link" href="/dashboard" target="_blank">Admin</a>
                    @else
                    <a class="nav-link" href="/login" target="_blank">Admin</a>
                    @endif
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="container-fluid text-center" style="background: #b70100;">
    <h4 class="text-decoration-none text-white p-2" style="margin-bottom:1px;">
        <marquee>Foxpost Próbamunka</marquee>
    </h4>

</div>
@yield('contents')
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>


</html>
