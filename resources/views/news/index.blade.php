@extends('layouts.app')
@section('contents')
<main class="p-0">
    <div class="container-fluid" style="background: #b70100;">
        <h1 class="text-white text-center p-5">Hírek</h1>
    </div>
    <div class="container">
        <div class="row">
            @if($news->count() > 0)
            @foreach($news as $post)
            <div class="col-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="small text-muted">{{ $post->created_at }}</div>
                        <h2 class="card-title">{{ $post->title }}</h2>
                        <p class="card-text">{{ Str::limit($post->description, 50) }}</p>
                        <a class="btn btn-default" href="/{{ $post->slug }}">Tovább olvasom →</a>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            Egyenlőre nincsenek híreink.
            @endif
        </div>
    </div>
</main>
@endsection
