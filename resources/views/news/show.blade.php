@extends('layouts.app')
@section('contents')
<main class="p-0">
    <div class="container-fluid text-center" style="background: #b70100;">
        <h1 class="text-white p-5">{{ $post->title }}</h1>
    </div>
    <div class="container">
        <small class="text-muted">{{ $post->created_at }}</small>
        <p class="mt-4">{{ $post->description }}</p>
    </div>
</main>

@endsection
